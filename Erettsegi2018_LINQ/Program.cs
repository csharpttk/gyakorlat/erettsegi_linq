﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Erettsegi2018_LINQ
{

    class Tadat
    {
        public int mikor { set; get; } //percekben
        public int ki { set; get; }    //azonosító
        public bool állapot { set; get; } //be => igaz
        public string óra_perccé_alakít()
        {
            return 9 + Convert.ToInt16(mikor / 60) + ":" + mikor % 60;
        }
    }
    class Thányszor
    {
        public int ki;
        public int hányszor;
    }
    class Tmikor
    {
        public int mikor;
        public int hányan;
    }
    class Program
    {
        
        static List<Tadat> társalgó = new List<Tadat>();
        static List<Thányszor> hányszormentát;
        static int személy;
        static void beolvasas()
        {
           társalgó=File.ReadAllLines("ajto.txt")
                           .Where(s=>s.Trim()!="")
                           .Select(sor => new Tadat { mikor=60*(Convert.ToInt16(sor.Split()[0])-9)+Convert.ToInt16(sor.Split()[1]),
                                          ki = Convert.ToInt16(sor.Split()[2]),
                                          állapot = sor.Split()[3] == "be"}).ToList();
           
        }
        static void első_utolsó()
        {
            int első = társalgó[0].ki;
            int utolsó = társalgó.Where(x => x.állapot == false).Last().ki;
            Console.WriteLine("2. feladat");
            Console.WriteLine("Az első belépő: {0}", első);
            Console.WriteLine("Az utolsó kilépő: {0}", utolsó);
        }
        static void kihanyszor()
        {
                 hányszormentát=társalgó.GroupBy(x => x.ki)
                .Select(x => new Thányszor{ ki = x.Key, hányszor = x.Count() })
                .OrderBy(x=>x.ki)
                .ToList();
            
            Console.WriteLine("3. feladat");          
            foreach (var személy in hányszormentát)
            {
                Console.WriteLine("{0} {1}", személy.ki, személy.hányszor); //később fileba
            }
        }
        static void bentmaradtak()
        {   //előző feladat feltölti kihányszormentát tömböt
            Console.WriteLine("4. feladat");
            Console.Write("A végén a társalgóban voltak: ");
            foreach  (Thányszor személy in hányszormentát)
            {
                if (személy.hányszor % 2 == 1)  //többször ment be, mint ki,  tehát bent van
                    Console.Write(személy.ki + " "); //később fileba
            }
            Console.WriteLine();
        }

        static void legtobben()
        {
            List<int> időpontok = társalgó.Select(x => x.mikor).Distinct().ToList();
            int bentlevők,maxbentlevők=-1, maxidő=-1;
            foreach (int eddig in időpontok)
            {
                bentlevők = társalgó.Where(x => x.mikor <= eddig).Where(xx => xx.állapot == true).Count() - társalgó.Where(x => x.mikor <= eddig).Where(X => X.állapot == false).Count();
                if (maxbentlevők < bentlevők) { maxbentlevők = bentlevők; maxidő = eddig; }
            }
            Console.WriteLine("5. feladat");
            Console.WriteLine("Például {0}-kor voltak legtöbben a teremben", (9 + maxidő / 60) + ":" + maxidő % 60);
            
        }
        static void személybeolvasás()
        {
            Console.WriteLine("6. feladat");
            Console.WriteLine("Adjon meg a személy azonosítóját!");
            személy = Convert.ToInt16(Console.ReadLine());
        }
        static void mettőlmeddig()
        {
            Console.WriteLine("7. feladat");
            foreach (var sz in társalgó.Where(x => x.ki == személy))
            {
                if (sz.állapot)
                {
                    Console.Write(sz.óra_perccé_alakít() + "-");
                }
                else
                {
                    Console.WriteLine(sz.óra_perccé_alakít());
                }
            }
            
           if (társalgó.Count(x=>x.ki==személy) % 2 == 1) Console.WriteLine();
        }
        static void összesenmennyit()
        {
            int mennyit = 0;
            int be = 0;

            foreach (var sz in  társalgó.Where(X=>X.ki==személy))
            {                
                    if (sz.állapot)
                    {
                        be = sz.mikor;
                    }
                    else
                    {
                        mennyit += sz.mikor - be;
                    }                
            }
            if (társalgó.Count(x => x.ki == személy) % 2 == 1) mennyit += 6 * 60 - be;
            Console.WriteLine("8. feladat");
            Console.WriteLine("A {0}. személy összesen {1} percet volt bent, a megfigyelés ideje alatt", személy, mennyit);
        }

        static void Main(string[] args)
        {
            beolvasas();
            első_utolsó();
            kihanyszor();
            bentmaradtak();
            legtobben();
            személybeolvasás();
            mettőlmeddig();
            összesenmennyit();
        }
    }

}
